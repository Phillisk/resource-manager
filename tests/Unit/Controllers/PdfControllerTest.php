<?php

namespace Tests\Unit\Controllers;

use App\Http\Resources\PdfResource;
use App\Models\Pdf;

class PdfControllerTest extends ControllerTest
{
    /**
     * Get the route name of the resource endpoint.
     *
     * @return string
     */
    protected function getCommonRouteName(): string
    {
        return 'pdfs';
    }

    /**
     * Get the API Resource.
     *
     * @return string
     */
    protected function getResource(): string
    {
        return PdfResource::class;
    }

    /**
     * Get the model.
     *
     * @return string
     */
    protected function getModel(): string
    {
        return Pdf::class;
    }

    /**
     * Get the store data.
     *
     * @return array
     */
    protected function getStoreData(): array
    {
        $pdf = Pdf::factory()->make();

        return $pdf->toArray();
    }

    /**
     * Get the update data.
     *
     * @return array
     */
    protected function getUpdateData(): array
    {
        $pdf = Pdf::factory()->make();

        return $pdf->toArray();
    }
}
