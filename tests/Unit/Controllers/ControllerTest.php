<?php

namespace Tests\Unit\Controllers;

use Tests\TestCase;

abstract class ControllerTest extends TestCase
{

    /**
     * Get the route name of the resource endpoint.
     *
     * @return string
     */
    abstract protected function getCommonRouteName(): string;
    /**
     * Get the API Resource.
     *
     * @return string
     */
    abstract protected function getResource(): string;

    /**
     * Get the data to be persisted.
     *
     * @return array
     */
    abstract protected function getStoreData(): array;

    /**
     * Get the update data.
     *
     * @return array
     */
    abstract protected function getUpdateData(): array;

    /**
     * Get the model.
     *
     * @return string
     */
    abstract protected function getModel(): string;

    /**
     * Extract the data from the resource
     *
     * @param JsonResource $resource
     *
     * @return array
     */
    protected function getResourceResponse($resource): array
    {
        $content = $resource->toResponse(request())->getContent();

        return json_decode($content, true);
    }

    /**
     * Make Index request.
     *
     * @param array  $fields
     * @param int    $page
     * @param int    $perPage
     * @param string $orderBy
     * @param array  $arrayResponse
     */
    protected function makeIndexRequest(
        $fields = [],
        array $arrayResponse = null
    ) {
        $payload = [];
        $endpoint = route($this->getCommonRouteName() . '.index');

        if (count($payload) > 0) {
            $endpoint = sprintf('%s?%s', $endpoint, http_build_query($payload));
        }

        $response = $this->json('GET', $endpoint);

        $response->assertStatus(200);

        $model = $this->getModel();

        $query = (new $model)->newQuery();

        $items = $query->get();

        $query = (new $model)->newQuery();

        if (!$arrayResponse) {
            $resource = $this->getResource();

            $content = $resource::collection($items);

            $response->assertJson($this->getResourceResponse($content));

            return;
        }

        $response->assertJson($arrayResponse);
    }


    /**
     * Test the store endpoint
     */
    public function testStore()
    {
        $endpoint = route($this->getCommonRouteName() . '.store');

        $data = $this->getStoreData();

        $response = $this->withoutExceptionHandling()->json('POST', $endpoint, $data);

        if ($response->baseResponse->status() !== 200) {
            dump(json_decode($response->baseResponse->getContent()));
        }
        $response->assertStatus(200);

        $model = $this->getModel();
        $created = (new $model)->orderBy('id', 'desc')->first()->toArray();

        foreach ($data as $key => $value) {
            $this->assertTrue($created[$key] === $value);
        }
    }

    /**
     * Test the show endpoint
     */
    public function testShow()
    {
        $model = $this->getModel();

        $this->withoutExceptionHandling()
            ->json('POST', route($this->getCommonRouteName() . '.store'), $this->getStoreData())
            ->assertSuccessful();

        $last = (new $model)->orderBy('id', 'desc')->first();

        $endpoint = route($this->getCommonRouteName() . '.show', $last->id);

        $response = $this->withoutExceptionHandling()->json('GET', $endpoint);

        $response->assertStatus(200);

        $resource = $this->getResource();

        $response->assertJson($this->getResourceResponse(new $resource($last)));
    }

    /**
     * Test the update endpoint
     */
    public function testUpdate()
    {
        $model = $this->getModel();

        $this->withoutExceptionHandling()
            ->json('POST', route($this->getCommonRouteName() . '.store'), $this->getStoreData())
            ->assertSuccessful();

        $created = (new $model)->orderBy('id', 'desc')->first();

        $endpoint = route($this->getCommonRouteName() . '.update', $created->id);

        $response = $this->withoutExceptionHandling()
            ->json('PUT', $endpoint, array_merge($this->getStoreData(), $this->getUpdateData()));

        $response->assertStatus(200);

        $created->refresh();

        $content = ['data' => ['message' => 'Updated Successfully']];

        $response->assertJson($content);
    }

    /*
     * Test the delete endpoint
     */
    public function testDestroy()
    {
        $model = $this->getModel();

        $this->withoutExceptionHandling()
            ->json('POST', route($this->getCommonRouteName() . '.store'), $this->getStoreData())
            ->assertSuccessful();

        $created = (new $model)->orderBy('id', 'desc')->first();

        $endpoint = route($this->getCommonRouteName() . '.destroy', $created->id);

        $response = $this->withoutExceptionHandling()->json('DELETE', $endpoint);

        $response->assertStatus(200);
    }
}
