<?php

namespace Tests\Unit\Controllers;

use App\Http\Resources\HtmlResource;
use App\Models\Html;

class HtmlControllerTest extends ControllerTest
{
    /**
     * Get the route name of the resource endpoint.
     *
     * @return string
     */
    protected function getCommonRouteName(): string
    {
        return 'htmls';
    }

    /**
     * Get the API Resource.
     *
     * @return string
     */
    protected function getResource(): string
    {
        return HtmlResource::class;
    }

    /**
     * Get the model.
     *
     * @return string
     */
    protected function getModel(): string
    {
        return Html::class;
    }

    /**
     * Get the store data.
     *
     * @return array
     */
    protected function getStoreData(): array
    {
        $html = Html::factory()->make();

        return $html->toArray();
    }

    /**
     * Get the update data.
     *
     * @return array
     */
    protected function getUpdateData(): array
    {
        $html = Html::factory()->make();

        return $html->toArray();
    }
}
