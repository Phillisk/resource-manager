<?php

namespace Tests\Unit\Controllers;

use App\Http\Resources\LinkResource;
use App\Models\Link;

class LinkControllerTest extends ControllerTest
{
    /**
     * Get the route name of the resource endpoint.
     *
     * @return string
     */
    protected function getCommonRouteName(): string
    {
        return 'links';
    }

    /**
     * Get the API Resource.
     *
     * @return string
     */
    protected function getResource(): string
    {
        return LinkResource::class;
    }

    /**
     * Get the model.
     *
     * @return string
     */
    protected function getModel(): string
    {
        return Link::class;
    }

    /**
     * Get the store data.
     *
     * @return array
     */
    protected function getStoreData(): array
    {
        $link = Link::factory()->make();

        return $link->toArray();
    }

    /**
     * Get the update data.
     *
     * @return array
     */
    protected function getUpdateData(): array
    {
        $link = Link::factory()->make();

        return $link->toArray();
    }
}
