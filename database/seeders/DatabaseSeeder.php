<?php

namespace Database\Seeders;

use App\Models\Html;
use App\Models\Link;
use App\Models\Pdf;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Html::factory(10)->create();
        Link::factory(10)->create();
        Pdf::factory(10)->create();
    }
}
