<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PdfResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'download_link' => $this->getDownloadLink(),
            'preview_link' => $this->getPreviewLink(),
            'created_at' => $this->formatDate($this->created_at),
        ];
    }

    /**
     * Format the date according to the timezone
     *
     */
    protected function formatDate($date): ?string
    {
        return (string) Carbon::parse($date)
            ->setTimezone(config('app.current_timezone'))
            ->format('Y-m-d H:i:s');
    }
}
