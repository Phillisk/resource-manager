<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * The current model.
     *
     * @var Illuminate\Database\Eloquent\Model;
     */
    protected $model;

    /**
     * The resource for the model.
     *
     * @var \Illuminate\Http\Resources\Json\JsonResource
     */
    protected $resource;

    /**
     * Create new instance
     */
    public function __construct()
    {
        $model = $this->getModel();
        $this->model = new $model;
        $this->resource = $this->getResource();
    }

    /**
     * Get the eloquent model to be used.
     *
     * @return string
     */
    abstract protected function getModel();

    /**
     * Get the current request
     *
     * @return
     */
    abstract protected function getRequest();
    
    /**
     * Get the current resource class
     *
     * @return
     */
    abstract protected function getResource();

    /*
    * Returns all items in the given model
    *
    */
    public function index()
    {
        $items = $this->model::latest()->paginate(10);

        return $this->resource::collection($items);
    }

    /*
    * stores a single item in given model
    *
    */
    public function store()
    {
        $request = app($this->getRequest());

        $model = $this->getModel();

        $this->model = new $model;

        $this->model->fill($request->validated());

        $this->prePersist();

        $this->model->save();

        $this->postPersist();

        return response()->json(['data' => ['message' => 'Saved Successfully']]);
    }

    /*
    * Returns a given item from the given model
    *
    */
    public function show($modelId)
    {
        $query = $this->model->newQuery();

        $this->preShow($modelId);

        $this->model = $query->findOrFail($modelId);

        $this->postShow();

        return new $this->resource($this->model);
    }

    /*
    * Update a single item in the given model
    *
    */
    public function update($modelId)
    {
        $request = app($this->getRequest());

        $this->model = $this->model->findOrFail($modelId);

        $this->model->fill($request->validated());

        $this->prePersist();

        $this->model->save();

        $this->postPersist();

        return response()->json(['data' => ['message' => 'Updated Successfully']]);
    }

    /*
    * Deletes an item from the given model
    *
    */
    public function destroy($modelId)
    {
        $this->model = $this->model->findOrFail($modelId);

        $this->preDestroy();

        $this->model->delete();

        $this->postDestroy();

        return response()->json(['data' => ['message' => 'Deleted Successfully']]);
    }

    /**
     * Pre show hook.
     */
    protected function preShow($modelId)
    {
        //
    }

    /**
     * Post show hook.
     */
    protected function postShow()
    {
        //
    }

    /**
     * Pre persist hook.
     */
    protected function prePersist()
    {
        //
    }

    /**
     * Post persist hook.
     */
    protected function postPersist()
    {
        //
    }

    /**
     * Pre Destroy hook.
     */
    protected function preDestroy()
    {
        //
    }

    /**
     * Post Destroy hook.
     */
    protected function postDestroy()
    {
        //
    }
}
