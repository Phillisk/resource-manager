<?php

namespace App\Http\Controllers;

use App\Models\Html;
use App\Http\Requests\HtmlRequest;
use App\Http\Resources\HtmlResource;

class HtmlController extends Controller
{
    /**
     * Get the form request to be used to validate the models
     *
     * @return string
     */
    protected function getRequest(): string
    {
        return HtmlRequest::class;
    }

    /**
     * Get the eloquent model to be used.
     *
     * @return string
     */
    protected function getModel(): string
    {
        return Html::class;
    }

    /**
    * Get the current resource class
    *
    * @return array
    */
    protected function getResource()
    {
        return HtmlResource::class;
    }
}
