<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Http\Requests\LinkRequest;
use App\Http\Resources\LinkResource;

class LinkController extends Controller
{
    /**
     * Get the form request to be used to validate the models
     *
     * @return string
     */
    protected function getRequest(): string
    {
        return LinkRequest::class;
    }

    /**
     * Get the eloquent model to be used.
     *
     * @return string
     */
    protected function getModel(): string
    {
        return Link::class;
    }

    /**
     * Get the current resource class
     *
     * @return array
     */
    protected function getResource()
    {
        return LinkResource::class;
    }
}
