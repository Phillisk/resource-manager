<?php

namespace App\Http\Controllers;

use App\Models\Pdf;
use App\Http\Requests\PdfRequest;
use App\Http\Resources\PdfResource;
use Illuminate\Support\Facades\Storage;

class PdfController extends Controller
{
    /**
     * Get the form request to be used to validate the models
     *
     * @return string
     */
    protected function getRequest(): string
    {
        return PdfRequest::class;
    }

    /**
     * Get the eloquent model to be used.
     *
     * @return string
     */
    protected function getModel(): string
    {
        return Pdf::class;
    }

    /**
     * Get the current resource class
     *
     * @return
     */
    protected function getResource()
    {
        return PdfResource::class;
    }

    /**
    * Pre persist hook.
    */
    protected function prePersist()
    {
        $path = $this->model->path ?? '';

        $file = request()->file('file');

        $this->model->path = $file ? $file->store('public') : $path;
    }

    /**
     * Get the Preview link
     * 
     * @param App/Models/Pdf $pdf
     * 
     * return jsonResponse
     */
    public function previewLink(Pdf $pdf)
    {
        abort_if(Storage::missing($pdf->path), 404);

        return response(Storage::get($pdf->path), 200)->header('Content-Type', 'application/pdf');
    }

    /**
     *  Get the Download link
     *  
     * @param App/Models/Pdf $pdf
     * 
     * return jsonResponse
     */
    public function downloadLink(Pdf $pdf)
    {
        abort_if(Storage::missing($pdf->path), 404);

        $size = Storage::size($pdf->path);

        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename='.$pdf->title.'.pdf',
            'Content-Length' => $size
        ];

        return Storage::download($pdf->path, $pdf->title.'.pdf', $headers);
    }

    /**
     * Replace file
     * 
     * @param App/Models/Pdf $pdf
     * 
     * return jsonResponse
     */
    public function replaceFile(Pdf $pdf)
    {
        $file = request()->file('file');
        $title = request()->get('title');

        $path = $file->store('public');

        $pdf->path = $path;
        $pdf->title = $title;
        $pdf->save();

        return response()->json(['data' => ['message' => 'File Replaced Successfully']]);
    }
}
