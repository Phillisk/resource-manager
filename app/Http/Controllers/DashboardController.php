<?php

namespace App\Http\Controllers;

use App\Models\Html;
use App\Models\Link;
use App\Models\Pdf;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class DashboardController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;


    /**
     * Get counts for all the resources
     *
     * @return json
     */
    public function getResourcesCount()
    {
        $pdfs = Pdf::count();
        $htmls = Html::count();
        $links = Link::count();

        return ['pdfs' => $pdfs, 'htmls' => $htmls, 'links' => $links];
    }
}
