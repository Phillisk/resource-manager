<?php

namespace App\Models;

use App\Services\PdfDownloadLink;
use App\Services\PdfPreviewLink;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pdf extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'pdfs';

    protected $fillable = [
        'title',
        'path',
    ];

    /**
     * @return string
     */
    public function getDownloadLink()
    {
        return app(PdfDownloadLink::class)->get($this);
    }

    /**
     * @return string
     */
    public function getPreviewLink()
    {
        return app(PdfPreviewLink::class)->get($this);
    }
}
