<?php

namespace App\Services;

use App\Models\Pdf;
use Illuminate\Support\Facades\Storage;

class PdfPreviewLink
{
    /**
     * @param App\Models\Pdf $pdf
     */
    public function get(Pdf $pdf, $inline = false): string
    {
        if (Storage::disk('local')->exists($pdf->path)) {
            return route('pdfs.preview', ['pdf' => $pdf->id]);
        }

        return '';
    }
}
