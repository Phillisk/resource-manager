<?php

use App\Http\Controllers\HtmlController;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/counts', 'App\Http\Controllers\DashboardController@getResourcesCount')
    ->name('resources.count');

Route::post('/pdfs/{pdf}/replace', 'App\Http\Controllers\PdfController@replaceFile')
    ->name('pdfs.replace');


Route::apiResources([
    'pdfs' => PdfController::class,
    'htmls' => HtmlController::class,
    'links' => LinkController::class,
]);
