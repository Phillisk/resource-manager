## Resource Manager

A simple app to manage htmls, links and pdfs. For management and visitors. No authentication required. Laravel + VueJs

The project uses Laravel Sail. In case you run into an error that port 80 is in use, kill the nginx process. 

The VueJs App is under a folder called ```clientapp```

## Api Installation

Please note that Laravel Sail uses docker and you will need to have it locally to run the project. See official documentation here: https://laravel.com/docs/8.x/installation#:~:text=a%20Laravel%20contributor.-,Your%20First%20Laravel%20Project,-We%20want%20it

After you clone this project, do the following:

1. cd into the project ```cd resource-manager```
2. copy env file and change the database password and username, change the urls as needed and the timezone  ```cp .env.example .env```
3. make sure your docker is running then run ```sail up``` to start the server
4. run ```sail artisan migrate``` to migrate the database
5. for artisan commands use ```sail artisan...```
6. to run tests change the database setting in the env file to testing, then ```sail artisan migrate``` and ```sail artisan test```

## Client Installation
1. cd into the folder ```cd clientapp```
2. copy env file and change the api url ```cp .env.example .env```
3. run ```yarn install```
4. to serve the app ```yarn serve```


