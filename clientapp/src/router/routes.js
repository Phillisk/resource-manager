import errors from '../components/errors/routes';
import htmls from '../components/htmls/routes';
import links from '../components/links/routes';
import pdfs from '../components/pdfs/routes';
import management from '../components/management/routes';
import visitor from '../components/visitor/routes';

export default [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "home" */ '@/components/Homepage.vue'),
    redirect: 'manage/dashboard',

    children: [...management, ...htmls, ...pdfs, ...links],
  },
  {
    path: '/visit',
    component: () =>
      import(/* webpackChunkName: "visitor" */ '@/components/visitor/VisitorPage.vue'),
    children: [...visitor],
  },

  // this is always last.
  ...errors,
];
