export default [
  {
    name: 'htmls.index',
    path: 'manage/htmls',
    component: () => import(/* webpackChunkName: "html-bundle" */ './HtmlsPage.vue'),
    props: { visitor: false },
  },
  {
    name: 'htmls.show',
    path: 'manage/htmls/:id',
    component: () => import(/* webpackChunkName: "html-bundle" */ './HtmlPage.vue'),
    props: { visitor: false },
  },
];
