export default [
  {
    path: '*',
    name: 'errors.404',
    component: () => import(/* webpackChunkName: "errors" */ './404Page.vue'),
  },
];
