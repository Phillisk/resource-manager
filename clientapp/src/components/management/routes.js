export default [
  {
    name: 'management.dashboard',
    path: '/manage/dashboard',
    component: () => import(/* webpackChunkName: "management-bundle" */ './ManagementPage.vue'),
  },
];
