export default [
  {
    name: 'links.index',
    path: 'manage/links',
    component: () => import(/* webpackChunkName: "link-bundle" */ './LinksPage.vue'),
    props: { visitor: false },
  },
];
