export default [
  {
    name: 'visitor.dashboard',
    path: '/dashboard',
    component: () => import(/* webpackChunkName: "visitor-bundle" */ './VisitorDashboard.vue'),
    props: { visitor: true },
  },
  {
    name: 'visitor.pdfs',
    path: '/pdfs',
    component: () => import(/* webpackChunkName: "visitor-bundle" */ '../pdfs/PdfsPage.vue'),
    props: { visitor: true },
  },
  {
    name: 'visitor.pdf',
    path: '/pdfs/:id',
    component: () => import(/* webpackChunkName: "visitor-bundle" */ '../pdfs/PdfPage.vue'),
    props: { visitor: true },
  },
  {
    name: 'visitor.htmls',
    path: '/htmls',
    component: () => import(/* webpackChunkName: "visitor-bundle" */ '../htmls/HtmlsPage.vue'),
    props: { visitor: true },
  },
  {
    name: 'visitor.html',
    path: '/htmls/:id',
    component: () => import(/* webpackChunkName: "visitor-bundle" */ '../htmls/HtmlPage.vue'),
    props: { visitor: true },
  },
  {
    name: 'visitor.links',
    path: '/links',
    component: () => import(/* webpackChunkName: "visitor-bundle" */ '../links/LinksPage.vue'),
    props: { visitor: true },
  },
];
