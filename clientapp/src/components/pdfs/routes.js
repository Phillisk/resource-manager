export default [
  {
    name: 'pdfs.index',
    path: 'manage/pdfs',
    component: () => import(/* webpackChunkName: "pdf-bundle" */ './PdfsPage.vue'),
    props: { visitor: false },
  },
  {
    name: 'pdfs.show',
    path: 'manage/pdfs/:id',
    component: () => import(/* webpackChunkName: "pdf-bundle" */ './PdfPage.vue'),
    props: { visitor: false },
  },
];
