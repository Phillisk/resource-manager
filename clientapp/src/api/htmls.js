/* eslint-disable no-unreachable */
import { http } from '@/plugins/http';
import { get } from '@/utils/get';

export default {
  async fetchAll(payload) {
    const response = await http.get(`/htmls?page=${payload.page}`);

    return get(response);
  },
  async fetchOne(payload) {
    const response = await http.get(`/htmls/${payload.id}`);

    return get(response);
  },
  async store(payload) {
    const response = await http.post('/htmls', payload);

    return get(response);
  },

  async update(payload) {
    const response = await http.put(`/htmls/${payload.id}`, payload);

    return get(response);
  },

  async destroy(payload) {
    const response = await http.delete(`/htmls/${payload.id}`);

    return get(response);
  },
};
