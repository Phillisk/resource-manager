import htmls from './htmls';
import links from './links';
import pdfs from './pdfs';
import home from './home';

export default {
  htmls,
  links,
  pdfs,
  home,
};
