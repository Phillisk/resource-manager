/* eslint-disable no-unreachable */
import { http } from '@/plugins/http';
import { get } from '@/utils/get';

export default {
  async getCounts() {
    const response = await http.get(`/counts`);

    return get(response);
  },
};
