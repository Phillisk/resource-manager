import { get as _get } from 'lodash';

export const get = (obj) => _get(obj, 'data');
