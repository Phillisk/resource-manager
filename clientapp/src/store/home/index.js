/* eslint-disable no-unused-vars */
import api from '../../api';

const API_NAMESPACE = 'home';

const state = {};

const getters = {};

const actions = {
  getCounts({ commit }, payload) {
    return api[API_NAMESPACE].getCounts(payload);
  },
};

const mutations = {};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
