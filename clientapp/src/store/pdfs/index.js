/* eslint-disable no-unused-vars */
import api from '../../api';
import commonState from '../common/state';
import commonMutations from '../common/mutations';
import commonActions from '../common/actions';

const API_NAMESPACE = 'pdfs';

const EMPTY_MODULE = {
  title: '',
  file: '',
};

const state = {
  ...commonState(EMPTY_MODULE),
};

const getters = {};

const actions = {
  ...commonActions(API_NAMESPACE),
};

const mutations = {
  ...commonMutations(EMPTY_MODULE),
};

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
};
