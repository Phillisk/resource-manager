export default (emptyEntity) => ({
  items: [],
  selected: { ...emptyEntity },
  formType: 'create',
});
