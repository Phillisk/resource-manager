import _set from 'lodash/set';
import {
  RESET_SELECTED,
  SET_SELECTED,
  SET_PROPERTY,
  SET_FORM_TYPE,
  REMOVE_ONE_OF_ITEMS_PROPERTY,
} from './types';

export default (emptyEntity) => ({
  [SET_SELECTED](state, payload) {
    state.selected = {
      ...payload,
    };
    state.formType = 'edit';
  },

  [RESET_SELECTED](state) {
    state.selected = {
      ...emptyEntity,
    };
    state.formType = 'create';
  },

  [SET_FORM_TYPE](state, payload) {
    state.formType = payload;
  },

  [SET_PROPERTY](state, { property, subProperty, value }) {
    if (subProperty) {
      state[property][subProperty] = value;
      return;
    }
    _set(state, property, value);
  },

  [REMOVE_ONE_OF_ITEMS_PROPERTY](state, payload) {
    const items = [...state.items.data];
    const index = items.findIndex((i) => i.id === payload.id);
    if (index === -1) {
      return;
    }
    items.splice(index, 1);
    state.items.data = items;
  },
});
