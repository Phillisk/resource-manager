/* eslint-disable no-unused-vars, no-shadow */
import api from '@/api';
import {
  RESET_SELECTED,
  SET_SELECTED,
  SET_PROPERTY,
  SET_FORM_TYPE,
  REMOVE_ONE_OF_ITEMS_PROPERTY,
} from './types';

export default (API_NAMESPACE) => ({
  fetchAll({ commit }, payload) {
    return api[API_NAMESPACE].fetchAll(payload).then(({ data, meta, links }) => {
      commit(SET_PROPERTY, { property: 'items', value: { data, meta, links } });

      return { data, meta, links };
    });
  },

  fetchOne({ commit }, payload) {
    return api[API_NAMESPACE].fetchOne(payload).then((response) => {
      commit(SET_SELECTED, response.data);

      return response;
    });
  },

  setProperty({ commit }, payload) {
    commit(SET_PROPERTY, payload);
  },

  resetSelected({ commit }) {
    commit(RESET_SELECTED);
  },

  setSelected({ commit }, payload) {
    commit(SET_SELECTED, payload);
  },

  setFormTypeCreate({ commit }) {
    commit(SET_FORM_TYPE, 'create');
  },

  setFormTypeEdit({ commit }) {
    commit(SET_FORM_TYPE, 'edit');
  },

  persist({ commit }, payload) {
    if (payload.formType === 'create') {
      return api[API_NAMESPACE].store(payload);
    }

    return api[API_NAMESPACE].update(payload);
  },

  destroy({ commit }, id) {
    return api[API_NAMESPACE].destroy({ id });
  },
});
