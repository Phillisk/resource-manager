import Vue from 'vue';
import Vuex from 'vuex';

import Htmls from './htmls';
import Links from './links';
import Pdfs from './pdfs';
import Home from './home';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    Htmls,
    Links,
    Pdfs,
    Home,
  },
  state: {},
  mutations: {},
  actions: {},
  plugins: [],

  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: process.env.DEV,
});
