import Vue from 'vue';
import App from './App.vue';

import router from './router';
import store from './store';
import VueCodemirror from 'vue-codemirror';
import Notifications from 'vue-notification';
import { BootstrapVue } from 'bootstrap-vue';

// import base style
import 'codemirror/lib/codemirror.css';

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import './app.css';

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);

Vue.use(Notifications);

Vue.use(VueCodemirror);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
