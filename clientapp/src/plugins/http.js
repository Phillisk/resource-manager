import axios from 'axios';
import { apiUrl } from '@/config';

export const http = axios.create({
  baseURL: `${apiUrl}`,
  withCredentials: true,
  headers: {
    Accept: 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
  },
});

export const cancelToken = () => axios.CancelToken;

export default ({ Vue }) => {
  Object.defineProperty(Vue.prototype, '$http', {
    get() {
      return http;
    },
  });
};

export const createAxios = (config) => {
  return axios.create({
    baseURL: `${apiUrl}${config.prefix}`,
    withCredentials: false,
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      Accept: 'application/json',
      ...config.headers,
    },
  });
};
